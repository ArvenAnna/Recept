export default function Sidebar() {
    'ngInject';

    return {
        controller: SidebarController,
        controllerAs: 'sidebarCtrl',
        templateUrl: '/frontend/html/sidebar/sidebar.html',
        bindings: {
            state: '=',
            query: '=',
            sort: '=',
            itemsPerPage: '=',
            receptId: '=',
            theme: '='
        }
    };
}

export function SidebarController($scope, stateService, sidebarService) {

    const vm = this;

    Object.assign(vm, {
        createRecept,
        editIngridients,
        parseFile,
        isUploadXmlMode,
        setSorting,
        setTheme,
        sortingOptions: {
            id: 'дата',
            name: 'алфавит'
        },
        themes: {
            fire: 'fire theme',
            wind: 'wind theme'
        },
        fonts: {
            'fireFont': 'fire font',
            'turFont': 'turandot font',
            'kolegraphyFont': 'kolegraphy font',
            'konkordRetroFont': 'konkord retro font',
            'aladinFont': 'aladin font',
            'americanRetroFont': 'american retro font',
            'andantinoFont': 'andantino font',
            'antikvarFont': 'antikvar font',
            'cansellaristFont': 'cansellarist font',
            'mixaFont': 'mixa font',
            'ownHandFont': 'own hand font',
            'panforteFont': 'panforte font'
        }
    });

    function setSorting(value) {
        vm.sort = value;
        $scope.$apply();
    }

    function setTheme(theme) {
        vm.theme = theme;
        $scope.$apply();
    }

    function isUploadXmlMode() {
        return !vm.file;
    }

    function parseFile() {
        if (vm.file) {
            sidebarService.parseReceptFromXML(vm.file)
                .then(data=> {
                    vm.file = null;
                    vm.receptId = data;
                    vm.state = stateService.states.RECEPT_DESCRIPTION;

                })
                .catch(reason => {
                    //vm.file = null;
                    vm.error = reason;
                });
        }
    }

    function createRecept() {
        vm.state = null; // for reseting data between two create states;
        vm.state = stateService.states.RECEPT_CREATE;
    }

    function editIngridients() {
        vm.state = stateService.states.INGRIDIENT_EDIT;
    }

}