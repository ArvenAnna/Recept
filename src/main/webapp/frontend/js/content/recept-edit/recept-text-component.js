export default function ReceptText() {
    'ngInject';

    return {
        controller: ReceptTextController,
        controllerAs: 'receptTextCtrl',
        templateUrl: '/frontend/html/content/edit/recept-text.html',
        bindings: {
            text: '='
        }
    };
}

export function ReceptTextController() {

    const vm = this;

    Object.assign(vm, {
        wrapP,
        wrapB,
        clear,
        formattedText
    });

    function formattedText() {
        $('#formatted').html(vm.text);
    }

    function wrapP() {
        replaceSelectedText('P');
        stripCrossTags('<P>', '</P>', /<P>|<\/P>/ig);
    }

    function wrapB() {
        replaceSelectedText('B');
        stripCrossTags('<B>', '</B>', /<B>|<\/B>/ig);
    }

    function clear() {
        replaceSelectedText('pofig sho', true);
        stripCrossTags('<P>', '</P>', /<P>|<\/P>/ig);
        stripCrossTags('<B>', '</B>', /<B>|<\/B>/ig);
    }

    function stripCrossTags(tag, antitag, pattern) {
        let result;
        let index = 0;
        while (result = pattern.exec(vm.text)) {
            if (index % 2 == 0 && result[0] === antitag) {
                let before = vm.text.substring(0, result.index);
                let after = vm.text.substring(pattern.lastIndex, vm.text.length);
                vm.text = before + after;
                stripCrossTags(tag, antitag, pattern);
                return;
            }
            if (index % 2 != 0 && result[0] === tag) {
                let before1 = vm.text.substring(0, result.index);
                let after1 = vm.text.substring(result.index, vm.text.length);
                vm.text = before1 + antitag + after1;
                stripCrossTags(tag, antitag, pattern);
                return;
            }
            //alert('Найдено: ' + result[0] + ' на позиции:' + result.index);
            //alert('Свойство lastIndex: ' + pattern.lastIndex);
            index++;
        }
    }

    function wrapTag(text, tag) {
        return '<' + tag + '>' + text + '</' + tag + '>';
    }

    function replaceSelectedText(tag, isDrop = false) {
        let text = $('#text');
        let obj = text[0];
        obj.focus();

        if (document.selection) {
            var s = document.selection.createRange();
            if (s.text) {
                s.text = wrapTag(s.text, tag);
                s.select();
                return true;
            }
        }
        else if (typeof(obj.selectionStart) == "number") {
            if (obj.selectionStart != obj.selectionEnd) {
                var start = obj.selectionStart;
                var end = obj.selectionEnd;
                var rs;
                if (isDrop) {
                    rs = stripTags(obj.value.substr(start, end - start));
                } else {
                    rs = wrapTag(obj.value.substr(start, end - start), tag);
                }
                obj.value = obj.value.substr(0, start) + rs + obj.value.substr(end);
                obj.setSelectionRange(end, end);
                vm.text = obj.value;
            }
            return true;
        }
        return false;
    }

    function stripTags(text) {
        let pattern = /<P>|<\/P>|<B>|<\/B>/ig;
        let result;
        while (result = pattern.exec(text)) {
            let before = text.substring(0, result.index);
            let after = text.substring(pattern.lastIndex, text.length);
            text = before + after;
        }
        return text;
    }


}
