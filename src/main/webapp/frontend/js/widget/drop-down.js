export default function DropDown($parse) {
    return {
        restrict: 'A',
        priority: 1,
        link: function (scope, element, attrs) {
            if (!attrs['dropDown']) {
                return;
            }
            const options = $parse(attrs['dropDown'])(scope);

            const input = $(element).children('input').first();
            const ul = $(element).children('ul').first();
            const lis = buildListItems();

            input.click(openDropDown);
            element.mouseleave(shutDropDown);
            lis.click(chooseListItem);

            function buildListItems() {
                Object.keys(options).map((objectKey) => {
                    const html = '<li>' + options[objectKey] + '</li>';
                    ul.append(html);
                });
                return ul.children('li');
            }

            function chooseListItem() {
                const liText = $(this).text();

                input.val(liText);

                shutDropDown();
                setListItemValue(convertValueToKey(liText));
            }

            function convertValueToKey(liText) {
                let key = null;
                Object.keys(options).forEach((objectKey) => {
                    if (options[objectKey] === liText) {
                        key = objectKey;
                    }
                })
                return key;
            }

            function shutDropDown() {
                ul.css('display', 'none');
            }

            function openDropDown() {
                ul.css('display', 'block');
            }

            function setListItemValue(value) {
                if (attrs['handler']) {
                    $parse(attrs['handler'])(scope)(value);
                }
            }
        }

    };
}