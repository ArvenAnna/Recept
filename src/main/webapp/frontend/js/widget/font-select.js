export default function FontSelect($parse) {
    return {
        restrict: 'A',
        priority: -1,
        link: function (scope, element, attrs) {
            setTimeout(fontSelect, 1000);

            function fontSelect() {
                const lis = $(element).children('ul').first().children('li');
                const fonts = $parse(attrs['fontSelect'])(scope);
                Object.keys(fonts).map(function (objectKey) {
                    lis.each((i, li) => {
                        if($(li).text() === fonts[objectKey]) {
                            $(li).css('font-family', objectKey);
                            $(li).bind('click', {param: objectKey}, setFont);
                        }
                    });
                });
            }

            function setFont(event) {
                const value = event.data.param;
                $('body').css('font-family', value);
                $('input').css('font-family', value);
                $('textarea').css('font-family', value);
                $('select').css('font-family', value);
            }
        }
    };
}